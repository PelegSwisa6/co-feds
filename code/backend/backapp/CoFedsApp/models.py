from django.db import models
from django.contrib.auth.models import User


class Customizeuser(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    phone = models.CharField(max_length=10, null=True)
    address = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.user.username


class Category(models.Model):
    category_id = models.IntegerField(primary_key=True)
    category_name = models.CharField(max_length=100)
    parent_id = models.ForeignKey(
        'self', on_delete=models.CASCADE, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.category_name



class Supplier(models.Model):
    supplier_id = models.IntegerField(primary_key=True)
    supplier_name = models.CharField(max_length=100)
    contact_name = models.CharField(max_length=100, null=True, blank=True)
    contact_email = models.EmailField(max_length=100, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.supplier_name


class Order(models.Model):
    order_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    list_products = models.TextField()
    total_price = models.DecimalField(max_digits=10, decimal_places=2)
    order_date = models.DateTimeField(auto_now_add=True)
    order_state = models.IntegerField(default=0)
    user_details = models.TextField(default="null")
    preparation_time = models.IntegerField(default=0)
    timeCollection = models.CharField(max_length=5, default="")



class Sales(models.Model):
    sale_name = models.CharField(max_length=255, null=True, blank=True)
    sale_code = models.IntegerField(primary_key=True, unique=True)
    quantity = models.PositiveIntegerField(default=0)
    sale_price = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)
    is_active = models.BooleanField(default=True)


class Product(models.Model):
    product_id = models.BigIntegerField(primary_key=True, unique=True)
    product_name = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.IntegerField(default=0)
    category_id = models.ForeignKey(Category, on_delete=models.CASCADE)
    supplier_id = models.ForeignKey(Supplier, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    image = models.CharField(max_length=255, null=True, blank=True)
    location = models.IntegerField(default=0)
    sale = models.ForeignKey(Sales, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.product_name


class Inventory(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=0)
    date = models.DateTimeField(auto_now_add=True)

class OpenCart(models.Model):
    cart_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    list_products = models.TextField()
    total_price = models.DecimalField(max_digits=10, decimal_places=2)

class BasicCart(models.Model):
    cart_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    list_products = models.TextField()
    total_price = models.DecimalField(max_digits=10, decimal_places=2)    