from rest_framework import serializers
from django.contrib.auth.models import User, Group
from rest_framework.authtoken.models import Token
from .models import Product, Order, Category, Inventory, Customizeuser


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class InventorySerializer(serializers.ModelSerializer):
    product_name = serializers.SerializerMethodField()

    class Meta:
        model = Inventory
        fields = '__all__'

    def get_product_name(self, obj):
        return obj.product.name


class UserSerilaizer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id","username", "first_name", "last_name", "email", "password"]
        extra_kwargs = {'password': {'write_only': True, 'required': True}}

    def create(self, validated_data):
        user = User.objects.create_user(first_name=validated_data['first_name'],
                                        last_name=validated_data['last_name'],
                                        username=validated_data['username'],
                                        email=validated_data['email'],
                                        password=validated_data['password']
                                        )
        Token.objects.create(user=user)
        group = Group.objects.get(name="client")
        group.user_set.add(user)
        return user


class CustomizeuserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customizeuser
        fields = ['user','user_id', 'phone', 'address']

    def create(self, validated_data):
        user_data = validated_data.pop('user')
        user = User.objects.create_user(**user_data)
        customizeuser = Customizeuser.objects.create(user=user, **validated_data)
        return customizeuser


# class UserSerilaizer(serializers.ModelSerializer):
#     class Meta:
#         model = User
#         fields = '__all__'
#         # extra_kwargs = {'password': {'write_only': True, 'required': True}}
#
#     def create(self, validated_data):
#         user = User.objects.create_user(**validated_data)
#         Token.objects.create(user=user)
#         return user
#

class OrderSerilaizer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'


class CategorySerilaizer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
