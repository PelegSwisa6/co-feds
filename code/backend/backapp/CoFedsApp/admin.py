from django.contrib import admin
from .models import Supplier, Product, Category, Customizeuser
from django.contrib.auth.models import Group, User


class Profileinline(admin.StackedInline):
    model = Customizeuser


class UserAdmin(admin.ModelAdmin):
    model = User
    fields = ["username", "first_name", "last_name", "email", "password"]
    inlines = [Profileinline]


# Register your models here.

admin.site.register(Supplier)
admin.site.register(Product)
admin.site.register(Category)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
