from django.urls import path, include
from rest_framework import routers
from .views import UserViewSet, CategoryViewSet, update_product_quantity, CustomAuthToken
from . import views
from .views import add_or_update_product
from rest_framework.authtoken.views import obtain_auth_token

router = routers.DefaultRouter()
router.register('users', UserViewSet)
router.register('category', CategoryViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('getcustomizeuser/', views.user_details_view),
    path('products/', views.products_view, name='products'),
    path('sales/', views.sales_view, name='sales'),
    path('supplier/', views.supplier_view),
    path('opencartview/', views.open_cart_view),
    path('basiccartview/', views.basic_cart_view),
    path('addcategory/', views.add_or_update_category),
    path('addsale/', views.add_or_update_sale),
    path('addproductstosale/', views.update_products_sale),
     path('changeuserdetails/', views.change_user_details),
    path('addsupplier/', views.add_or_update_supplier),
    path('orders/', views.orders_view, name='orders'),
    path('updateorder/', views.changeOrderState, name='products'),
    path('products/<int:product_id>/update_quantity/',
         update_product_quantity, name='update_quantity'),
    path('order/', views.create_order),
    path('opencart/', views.save_open_cart,name='opencarts'),
    path('basiccart/', views.save_basic_cart,name='basiccart'),
    path('products/<int:product_id>/', views.add_or_update_product),
    path('addproducts/<int:product_id>/', views.add_product_quantity),
    path('checkuser/', CustomAuthToken.as_view(), name='check_user'),
    path('outcomingstock/', views.get_supplier_inventory),
    path('supplierorder/', views.get_supplier_order),
    path('customizeuser/', views.CustomizeuserCreateView.as_view(), name='customizeuser_create'),
    path('customizeuser/list/', views.CustomizeuserListView.as_view(), name='customizeuser_list'),
    path('activate/<uidb64>/<token>', views.activate, name='activate'),


]
