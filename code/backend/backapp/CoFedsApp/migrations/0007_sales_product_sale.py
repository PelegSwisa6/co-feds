# Generated by Django 4.1.7 on 2023-05-02 15:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('CoFedsApp', '0006_order_timecollection'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sales',
            fields=[
                ('sale_name', models.CharField(blank=True, max_length=255, null=True)),
                ('sale_code', models.IntegerField(primary_key=True, serialize=False, unique=True)),
            ],
        ),
        migrations.AddField(
            model_name='product',
            name='sale',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='CoFedsApp.sales'),
        ),
    ]
