from django.apps import AppConfig


class CofedsappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'CoFedsApp'
