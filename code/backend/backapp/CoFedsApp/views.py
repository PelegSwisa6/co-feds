from django.contrib.auth import get_user_model
from django.http import JsonResponse, HttpResponse
from rest_framework import viewsets, generics
from rest_framework import status
from .serializers import UserSerilaizer, ProductSerializer, CategorySerilaizer, InventorySerializer, CustomizeuserSerializer
from django.shortcuts import get_object_or_404, redirect
from django.views.decorators.csrf import csrf_exempt
from .models import Product, Category, Supplier, Order, Inventory, Sales, Customizeuser,OpenCart,BasicCart
from django.contrib.auth.models import User
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer
from .permissions import IsEmployee
import json
from datetime import datetime
from django.utils.encoding import force_bytes, force_str
from .token import account_activation_token
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.core.mail import EmailMessage
import pandas as pd
from dateutil.relativedelta import relativedelta

def activate(request, uidb64, token):
    User = get_user_model()
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except:
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save(update_fields=['is_active'])
    return HttpResponse("החשבון הופעל בהצלחה")



def activateEmail(request, user, to_email):
    mail_subject = "מייל לאימות חשבון"
    message = render_to_string("template_activate_account.html", {
        'user': user.username,
        'domain': get_current_site(request).domain,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        'token': account_activation_token.make_token(user),
        "protocol": 'https' if request.is_secure() else 'http'
    })
    email = EmailMessage(mail_subject, message, to=[to_email])
    if email.send():
        HttpResponse(f'Dear <b>{user}</b>, please go to you email <b>{to_email}</b> inbox and click on \
                received activation link to confirm and complete the registration. <b>Note:</b> Check your spam folder.')

    else:
        HttpResponse(f'Problem sending email to {to_email}, check if you typed it correctly.')


class CustomizeuserListView(generics.ListAPIView):
    queryset = Customizeuser.objects.all()
    serializer_class = CustomizeuserSerializer
    permission_classes = [IsAuthenticated]


class CustomizeuserCreateView(generics.CreateAPIView):
    queryset = Customizeuser.objects.all()
    serializer_class = CustomizeuserSerializer

    def post(self, request, *args, **kwargs):
        user_data = request.data.get('user')
        username = user_data.get('username')
        if User.objects.filter(username=username).exists():
            return JsonResponse({'success': False, 'message': 'כבר קיים משתמש עם שם משתמש זה'}, status=status.HTTP_400_BAD_REQUEST)
        email = user_data.get('email')
        if User.objects.filter(email=email).exists():
            return JsonResponse({'success': False, 'message': 'כבר קיים משתמש עם מייל זה'}, status=status.HTTP_400_BAD_REQUEST)

        try:
            user = User.objects.get(username=user_data['username'])
        except User.DoesNotExist:
            user_serializer = UserSerilaizer(data=user_data)
            if user_serializer.is_valid():
                user = user_serializer.save()
                user.is_active = False
                user.save(update_fields=['is_active'])
                activateEmail(request, user, user_data['email'])
                token = Token.objects.filter(user=user).first()
                if not token:
                    Token.objects.create(user=user)

            else:
                return JsonResponse({'success': False, 'message': 'מידע שעברת מותעה'}, status=status.HTTP_400_BAD_REQUEST)

        try:
            customizeuser = Customizeuser.objects.get(user=user)
            customizeuser_serializer = CustomizeuserSerializer(customizeuser, data=request.data)
        except Customizeuser.DoesNotExist:
            # Create a new customizeuser with the provided data and the user object
            customizeuser_serializer = CustomizeuserSerializer(
                data={
                    **request.data,
                    'user': user.id
                }
            )
            phoneNumber = request.data.get('phone')
            address = request.data.get('address')
            address_str = json.dumps(address)
            user = User.objects.get(pk=user.id)
            cus = Customizeuser.objects.create(phone=phoneNumber, address=address_str, user=user)
            if customizeuser_serializer.is_valid():
                customizeuser_serializer.save()

            return JsonResponse({'success': True, 'message': 'The order has been received in the system'},
                                status=status.HTTP_200_OK)
        else:
            return JsonResponse({'success': False, 'message': 'סליחה משהו ישתבש נסה שנית'},
                                status=status.HTTP_400_BAD_REQUEST)


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerilaizer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerilaizer


class CustomAuthToken(APIView):
    def post(self, request, *args, **kwargs):
        serializer = AuthTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'id': user.id,
            'group': user.groups.first().name if user.groups.exists() else None
        }, status=status.HTTP_200_OK)


@api_view(['GET'])
def products_view(request):
    products = Product.objects.all()

    data = [{'id': p.product_id, 'name': p.product_name, 'price': p.price, 'quantity': p.quantity,
             "supplier": p.supplier_id_id, "category": p.category_id_id, "image": p.image, "location": p.location,
             "sale_id": p.sale_id}
            for p in products]
    return Response(data)


@api_view(['GET'])
def sales_view(request):
    sales = Sales.objects.all()

    data = [{'sale_name': s.sale_name, 'sale_code': s.sale_code, 'quantity': s.quantity, 'sale_price': s.sale_price,
             'is_active': s.is_active, 'location': 999}
            for s in sales]
    return Response(data)

@api_view(['GET'])
def user_details_view(request):
    user_details = Customizeuser.objects.all()
    data = [{'user_id': d.user_id, 'phone': d.phone, 'address': d.address}
            for d in user_details]
    return Response(data)


@api_view(['GET'])
def orders_view(request):
    orders = Order.objects.all()
    data = [{'id': o.order_id, 'product_list': o.list_products, 'price': o.total_price, 'user_id': o.user_id,
             'order_state': o.order_state, 'user_details': o.user_details, 'preparation_time': o.preparation_time,
             'timeCollection': o.timeCollection}
            for o in orders]
    return Response(data)


@api_view(['GET'])
def categorys_view(request):
    category = Category.objects.all()
    data = [{'id': c.category_id, 'name': c.category_name}
            for c in category]
    return Response(data)


@api_view(['GET'])
def open_cart_view(request):
    open_cart = OpenCart.objects.all()
    data = [{'id': c.cart_id,'list_products': c.list_products, 'user_id': c.user_id}
            for c in open_cart]
    return Response(data)

@api_view(['GET'])
def basic_cart_view(request):
    basic_cart = BasicCart.objects.all()
    data = [{'id': b.cart_id,'list_products': b.list_products, 'user_id': b.user_id}
            for b in basic_cart]
    return Response(data)

@api_view(['PUT'])
def update_product_quantity(request, product_id):
    product = get_object_or_404(Product, product_id=product_id)

    quantity = request.data.get('quantity')

    if quantity is not None:

        quantity = int(quantity)

        if quantity > product.quantity:
            return JsonResponse({'error': 'requested quantity is greater than available quantity'},
                                status=status.HTTP_400_BAD_REQUEST)

        product.quantity -= quantity
        product.save()

        inventory = Inventory(product=product, quantity=quantity)
        inventory.save()

        serializer = ProductSerializer(product)
        return JsonResponse(serializer.data, status=status.HTTP_200_OK)
    else:

        return JsonResponse({'error': 'quantity cannot be null'}, status=status.HTTP_400_BAD_REQUEST)


@csrf_exempt
@api_view(['POST'])
def add_product_quantity(request, product_id):
    quantity = request.data.get('quantity')

    if request.method == 'POST':

        product = Product.objects.get(product_id=product_id)
        product.quantity += quantity
        product.save()

        return JsonResponse({'success': True, 'message': 'כמות המוצר עודכנה בהצלחה'},
                            status=status.HTTP_200_OK)
    else:
        return JsonResponse({'success': False, 'message': 'Invalid request method'}, status=status.HTTP_400_BAD_REQUEST)


@csrf_exempt
@api_view(['POST'])
def add_or_update_product(request, product_id):
    if request.method == 'POST':
        category_id = request.data.get('category_id')
        category = Category.objects.get(pk=category_id)
        supplier_id = request.data.get('supplier_id')
        supplier = Supplier.objects.get(pk=supplier_id)
        product_name = request.data.get('product_name')
        quantity = request.data.get('quantity')
        price = request.data.get('price')
        location = request.data.get('location')
        image = request.data.get('image')

        try:
            # Check if the product already exists
            product = Product.objects.get(product_id=product_id)
            product.product_name = product_name
            product.category_id_id = category
            product.supplier_id = supplier
            product.quantity = quantity
            product.price = price
            product.location = location
            product.image = image
        except Product.DoesNotExist:
            # Create a new product
            product = Product(product_id=product_id, product_name=product_name,
                              category_id=category, supplier_id=supplier,
                              quantity=quantity, price=price, location=location, image=image)
        product.save()
        return JsonResponse({'success': True, 'message': 'המוצר נוסף / עודכן בהצלחה'},
                            status=status.HTTP_200_OK)
    else:
        return JsonResponse({'success': False, 'message': 'הייתה בעיה בעדכון המוצר אנא נסה שוב'},
                            status=status.HTTP_400_BAD_REQUEST)


@csrf_exempt
@api_view(['POST'])
def update_products_sale(request):
    product_id = request.data.get("productID")
    sale_id = request.data.get('saleID')
    product = Product.objects.get(product_id=product_id)
    if sale_id != 0:
        product.sale_id = sale_id
    else:
        product.sale_id = ""
    product.save()
    return JsonResponse({'success': True, 'message': 'המוצרים התווספו / הוסרו מהמבצע בהצלחה'},
                        status=status.HTTP_200_OK)


@csrf_exempt
@api_view(['POST'])
def add_or_update_sale(request):
    if request.method == 'POST':
        sale_name = request.data.get('saleName')
        sale_code = request.data.get('saleCode')
        quantity = request.data.get('quantity')
        sale_price = request.data.get('salePrice')
        is_active = request.data.get('IsActive')

        try:
            sale = Sales.objects.get(sale_code=sale_code)
            sale.sale_name = sale_name
            sale.quantity = quantity
            sale.sale_price = sale_price
            sale.is_active = is_active

        except Sales.DoesNotExist:
            sale = Sales(sale_name=sale_name, sale_code=sale_code, quantity=quantity, sale_price=sale_price,
                         is_active=is_active)
        sale.save()
        return JsonResponse({'success': True, 'message': 'המבצע התעדכן בהצלחה'}, status=status.HTTP_200_OK)
    else:
        return JsonResponse({'success': False, 'message': 'Invalid request method'}, status=status.HTTP_400_BAD_REQUEST)


@csrf_exempt
@api_view(['POST'])
def add_or_update_category(request):
    if request.method == 'POST':
        category_id = request.data.get('categoryID')
        category_name = request.data.get('categoryName')

        try:
            category = Category.objects.get(category_id=category_id)
            category.category_name = category_name

        except Category.DoesNotExist:

            category = Category(category_id=category_id, category_name=category_name)
        category.save()
        return JsonResponse({'success': True, 'message': "הקטגוריה נוספה או עודכנה בהצלחה"}, status=status.HTTP_200_OK)
    else:
        return JsonResponse({'success': False, 'message': 'Invalid request method'}, status=status.HTTP_400_BAD_REQUEST)


@csrf_exempt
@api_view(['POST'])
def change_user_details(request):
    if request.method == 'POST':
        user_id = request.data.get('UserId')
        first_name = request.data.get('firstName')
        last_name = request.data.get('lastName')
        email = request.data.get('email')
        phone = request.data.get('phone')
        address = request.data.get('address')


       
        user = User.objects.get(id=user_id)
        user_details = Customizeuser.objects.get(user_id=user_id)
        user.first_name = first_name
        user.last_name = last_name
        user.email = email
        user_details.phone = phone
        user_details.address = address

        user.save()     
        user_details.save()     

        return JsonResponse({'success': True, 'message':"הקטגוריה נוספה או עודכנה בהצלחה"}, status=status.HTTP_200_OK)
    else:
        return JsonResponse({'success': False, 'message': 'Invalid request method'}, status=status.HTTP_400_BAD_REQUEST)

@csrf_exempt
@api_view(['POST'])
def add_or_update_supplier(request):
    if request.method == 'POST':
        supplier_id = request.data.get('supplierID')
        supplier_name = request.data.get('supplierName')
        contact_name = request.data.get('contactName')
        contact_email = request.data.get('contactEmail')

        try:
            supplier = Supplier.objects.get(supplier_id=supplier_id)
            supplier.supplier_name = supplier_name
            supplier.contact_name = contact_name
            supplier.contact_email = contact_email

        except Supplier.DoesNotExist:

            supplier = Supplier(supplier_id=supplier_id, supplier_name=supplier_name, contact_name=contact_name,
                                contact_email=contact_email)
        supplier.save()
        return JsonResponse({'success': True, "message": "הספק נוסף או עודכן בהצלחה"}, status=status.HTTP_200_OK)
    else:
        return JsonResponse({'success': False, 'message': 'Invalid request method'}, status=status.HTTP_400_BAD_REQUEST)


@csrf_exempt
@api_view(['POST'])
def changeOrderState(request):
    if request.method == 'POST':
        current_state = request.data.get('order_state')
        order_id = request.data.get('order_id')
        order = Order.objects.get(order_id=order_id)
        userdata = json.loads(order.user_details)
        user_name =userdata['name']
        to_email = userdata['email']

        if (current_state == 0):
            order.order_state = 1
        elif (current_state == 1):
            order.order_state = 2
        order.save()
        readyOrderEmail(request,to_email,order_id,user_name )
        return JsonResponse({'success': True}, status=status.HTTP_200_OK)
    else:
        return JsonResponse({'success': False}, status=status.HTTP_400_BAD_REQUEST)
def readyOrderEmail(request,to_email ,order_id ,user_name):
    mail_subject = "הזמנת מוכנה"
    message = render_to_string("tamplate_ready_order.html", {
        'orderid': order_id,
        'user_name':user_name,
    })
    email = EmailMessage(mail_subject, message, to=[to_email])
    if email.send():
        HttpResponse(f'Dear client, please go to you email <b>{to_email}</b> inbox and click on \
                received activation link to confirm and complete the registration. <b>Note:</b> Check your spam folder.')

    else:
        HttpResponse(f'Problem sending email to {to_email}, check if you typed it correctly.')



@csrf_exempt
@api_view(['POST'])
def create_order(request):
    if request.method == 'POST':
        user_id = request.data.get('user_id')
        user = User.objects.get(pk=user_id)
        product_list = request.data.get('products')
        total_price = request.data.get('total_price')
        user_details = request.data.get('userDetails')
        preparation_time = request.data.get('prepartionTime')
        collectionTime = request.data.get('collectionTime')
        product_list_str = json.dumps(product_list)
        user_details_str = json.dumps(user_details)

        order = Order.objects.create(
            list_products=product_list_str,
            total_price=total_price,
            order_date=datetime.now(),
            user=user,
            user_details=user_details_str,
            preparation_time=preparation_time,
            timeCollection=collectionTime

        )



        to_email=user_details.get('email')
        orderid= order.order_id
        total= "{:.2f}".format(float(order.total_price))
        filtered_data = [item for item in product_list if 'sale' not in item]
        df = pd.DataFrame(filtered_data)
        shoplist  = df.rename(columns={'id': 'ברקוד','name': 'שם     ','quantity':'כמות',"location":"מיקום"})[['ברקוד',  'כמות', 'מיקום','שם     ']]
        shoplistEmail(request,to_email ,orderid ,shoplist,total,collectionTime)
        return JsonResponse({'success': True, 'message': 'ההזמנה התקבלה ותהיה מוכנה לאיסוף בשעה שנבחרה'},
                            status=status.HTTP_200_OK)
    else:
        return JsonResponse({'success': False, 'message': 'The order was not received, please try again'},
                            status=status.HTTP_400_BAD_REQUEST)

def shoplistEmail(request,to_email ,orderid ,shoplist,total,collectionTime):
    mail_subject = "קבלת קניות"
    message = render_to_string("template_shoping_list.html", {
        'orderid': orderid,
        'shoplist':shoplist,
        'total':total,
        'collectionTime':collectionTime,
    })
    email = EmailMessage(mail_subject, message, to=[to_email])
    if email.send():
        HttpResponse(f'Dear client, please go to you email <b>{to_email}</b> inbox and click on \
                received activation link to confirm and complete the registration. <b>Note:</b> Check your spam folder.')

    else:
        HttpResponse(f'Problem sending email to {to_email}, check if you typed it correctly.')


@csrf_exempt
@api_view(['POST'])
def save_open_cart(request):
    if request.method == 'POST':
        user_id = request.data.get('user_id')
        user = User.objects.get(pk=user_id)
        product_list = request.data.get('products')
        total_price = request.data.get('total_price')
        product_list_str = json.dumps(product_list)
        try:
            open_cart  = OpenCart.objects.get(user=user)
            open_cart.list_products=product_list_str
            open_cart.total_price =total_price
            

        except OpenCart.DoesNotExist:
            open_cart = OpenCart.objects.create(
            list_products=product_list_str,
            total_price=total_price,
            user=user,
        )
        open_cart.save()
        return JsonResponse({'success': True, 'message': 'הרשימה נוספה לעגלה'},
                            status=status.HTTP_200_OK)
    else:
        return JsonResponse({'success': False, 'message': ''},
                            status=status.HTTP_400_BAD_REQUEST)
        
@csrf_exempt
@api_view(['POST'])
def save_basic_cart(request):
    if request.method == 'POST':
        user_id = request.data.get('user_id')
        user = User.objects.get(pk=user_id)
        product_list = request.data.get('products')
        total_price = request.data.get('total_price')
        product_list_str = json.dumps(product_list)
        try:
            basic_cart  = BasicCart.objects.get(user=user)
            basic_cart.list_products=product_list_str
            basic_cart.total_price =total_price
            

        except BasicCart.DoesNotExist:
            basic_cart = BasicCart.objects.create(
            list_products=product_list_str,
            total_price=total_price,
            user=user,
        )
        basic_cart.save()       

        return JsonResponse({'success': True, 'message': 'הרשימה נוספה לעגלה'},
                            status=status.HTTP_200_OK)
    else:
        return JsonResponse({'success': False, 'message': ''},
                            status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET'])
def supplier_view(request):
    supplier = Supplier.objects.all()

    data = [{'id': s.supplier_id, 'name': s.supplier_name, 'contact': s.contact_name, ' c_mail': s.contact_email}
            for s in supplier]
    return Response(data)


@api_view(['GET'])
def category_view(request):
    category = Category.objects.all()

    data = [{'id': c.category_id, 'name': c.category_name, }
            for c in category]
    return Response(data)


@api_view(['GET'])
def get_supplier_inventory(request):
    start_date = request.GET.get('start_date')
    end_date = request.GET.get('end_date')

    date_obj = datetime.strptime(end_date, "%m-%d-%Y")
    formatted_date_str = date_obj.strftime("%Y-%m-%d")
    end_date = formatted_date_str
    date_obj = datetime.strptime(start_date, "%m-%d-%Y")
    formatted_date_str = date_obj.strftime("%Y-%m-%d")
    start_date = formatted_date_str

    supplier_id = request.GET.get('supplier_id')
    supplier_id = int(supplier_id)

    inventory_records = Inventory.objects.filter(date__gte=start_date, date__lte=end_date + ' 23:59:59')

    products = {}
    for record in inventory_records:

        if record.product in products:
            products[record.product] += record.quantity
        else:
            products[record.product] = record.quantity
    if supplier_id == 9999:
        supplier_products = {product: quantity for product, quantity in products.items() if product.supplier_id.pk != 8}

    else:
        supplier_products = {product: quantity for product, quantity in products.items() if
                             product.supplier_id.pk == supplier_id}

    products_list = [{"product": key.product_id, "quantity": value, "product_name": key.product_name} for key, value in
                     supplier_products.items()]

    return JsonResponse(products_list, safe=False)


@api_view(['GET'])
def get_supplier_order(request):
    start_date = request.GET.get('start_date')
    end_date = request.GET.get('end_date')

    date_obj = datetime.strptime(end_date, "%m-%d-%Y")
    formatted_date_str = date_obj.strftime("%Y-%m-%d")
    end_date = formatted_date_str
    date_obj = datetime.strptime(start_date, "%m-%d-%Y")
    formatted_date_str = date_obj.strftime("%Y-%m-%d")
    start_date = formatted_date_str

    supplier_id = request.GET.get('supplier_id')
    supplier_id = int(supplier_id)

    inventory_records_2023 = Inventory.objects.filter(date__gte=start_date, date__lte=end_date + ' 23:59:59')

    products_2023 = {}
    for record in inventory_records_2023:
        if record.product in products_2023:
            products_2023[record.product] += record.quantity
        else:
            products_2023[record.product] = record.quantity

   
    supplier_products = {product: quantity for product, quantity in products_2023.items() if product.supplier_id.pk == supplier_id}
    previous_month_start = (date_obj - relativedelta(years=1)).replace(day=1)
    previous_month_end = (date_obj - relativedelta(years=1)).replace(day=1) + relativedelta(day=31)
    inventory_records_2022 = Inventory.objects.filter(date__gte=previous_month_start, date__lte=previous_month_end, product__supplier_id=supplier_id)

    products_2022 = {}
    for record in inventory_records_2022:
        if record.product in products_2022:
            products_2022[record.product] += record.quantity
        else:
            products_2022[record.product] = record.quantity

    products_list = []
    for key, value in supplier_products.items():
        if key.product_id in products_2022:
            products_list.append({"product": key.product_id, "quantity": value, "product_name": key.product_name})
        else:
            products_list.append({"product": key.product_id, "quantity": value, "product_name": key.product_name})

    for key, value in products_2022.items():
        if key not in supplier_products:
            products_list.append({"product": key.product_id, "quantity": value, "product_name": key.product_name})

    return JsonResponse(products_list, safe=False)

