"""
WSGI config for backapp project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.1/howto/deployment/wsgi/
"""

import os
import sys

from django.core.wsgi import get_wsgi_application

SERVER_BASE = '/var/www/co-feds/server'
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'backapp.settings')

sys.path.append(SERVER_BASE)
sys.path.append(f'{SERVER_BASE}/backapp')
sys.path.append(f'{SERVER_BASE}/CoFedsApp')


application = get_wsgi_application()
