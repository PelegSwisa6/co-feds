from django.conf import settings
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from rest_framework.authtoken.views import obtain_auth_token

api_prefix = settings.API_PREFIX

urlpatterns = [
    path('admin/', admin.site.urls),
    path(f'{api_prefix}', include('CoFedsApp.urls')),
    path('api/', include('django.contrib.auth.urls')),
    path('auth/', obtain_auth_token),
]
