FULL STACK PROJECT

link - https://exlab-sapir.com:8082/

We will work on the project as a team on two websites and connect between them:

1. We will create an interface for the business to allow it to manage inventory and prepare orders in the best and fastest way.
2. We will create a website where the customer can place orders and choose a time when he wants to pick them up.

In the project we will write code in React to create and handle the client side, in Django to handle the server side and the api requests and in MySQL to save all our data.
